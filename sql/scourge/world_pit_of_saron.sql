﻿update creature_template set ScriptName = '' where entry IN (36765, 36771);

UPDATE creature_template SET scriptname = "pos_intro" WHERE entry IN (36990, 36993);
UPDATE gameobject_template SET Scriptname = 'go_prisoner_corePS' WHERE entry IN (201969, 202168);

delete from areatrigger_scripts where entry = 5578;
INSERT INTO `areatrigger_scripts` (`entry`,`ScriptName`) VALUES
(5578,'at_ymirjar_flamebearer_pos');
DELETE FROM `areatrigger_scripts` where `entry`= 5579;
INSERT INTO `areatrigger_scripts` (`entry`,`ScriptName`) VALUES
(5579,'at_fallen_warrior_pos');
DELETE FROM `areatrigger_scripts` where `entry`= 5580;
INSERT INTO `areatrigger_scripts` (`entry`,`ScriptName`) VALUES
(5580,'at_ice_cicle_pos');
DELETE FROM `areatrigger_scripts` where `entry`= 5598;
INSERT INTO `areatrigger_scripts` (`entry`,`ScriptName`) VALUES
(5598,'at_slave_rescued_pos');
DELETE FROM `areatrigger_scripts` where `entry`= 5599;
INSERT INTO `areatrigger_scripts` (`entry`,`ScriptName`) VALUES
(5599,'at_geist_ambusher_pos');

UPDATE creature_template SET Scriptname = 'pos_outro' WHERE entry IN (38189, 38188);
UPDATE creature_template set Scriptname = '' where entry = 36886;

SET @ENTRY1=36840;
SET @ENTRY2=36892;
SET @ENTRY3=36893;
SET @ENTRY4=36841;
SET @ENTRY5=36842;
SET @ENTRY6=37584;
SET @ENTRY7=37588;
SET @ENTRY8=37587;
SET @ENTRY9=37496;
SET @ENTRY10=37497;
SET @ENTRY11=37729;
SET @ENTRY12=37728;
SET @ENTRY13=36877;
DELETE FROM creature WHERE id IN (@ENTRY1, @ENTRY2, @ENTRY3, @ENTRY4, @ENTRY5, @ENTRY6, @ENTRY7, @ENTRY8, @ENTRY9, @ENTRY10, @ENTRY11, @ENTRY12, @ENTRY13);

update creature_template set unit_flags = 32832, faction_A = 21, faction_H = 21 where entry IN(36830, 36892);

UPDATE gameobject_template set flags = 1, faction = 1375 WHERE entry = 201848;

DELETE FROM script_texts WHERE entry IN (-1658074, -1658075, -1658076, -1658077, -1658078, -1658080, -1658081, -1658082, -1658084, -1658085, -1658086, -1658087, -1658088);
INSERT INTO `script_texts` (`npc_entry`,`entry`,`content_default`,`sound`,`type`,`language`,`emote`,`comment`) VALUES 
('0', '-1658074', 'Intruders have entered the masters domain. Signal the alarms!', 16747, 1, 0, 0, 'Tyrannus Opening'),
('0', '-1658075', 'Hmph fodder Not even fit to labor in the quarry. Relish these final moments for soon you will be nothing more than mindless undead', 16748, 1, 0, 0, 'Tyrannus Opening'),
('0', '-1658076', 'Soldiers of the Horde, attack!', 17045, 1, 0, 0, 'Sylvanas Opening'),
('0', '-1658077', 'Heroes of the Alliance, attack!', 16626, 1, 0, 0, 'Jaina Opening'),
('0', '-1658078', 'Your last waking memory will be of agonizing pain', 16749, 1, 0, 0, 'Tyrannus Opening'),
('0', '-1658080', 'Pathetic weaklings', 17046, 1, 0, 0, 'Sylvanas Opening'),
('0', '-1658081', 'NO! YOU MONSTER!', 16627, 1, 0, 0, 'Jaina Opening'),
('0', '-1658082', 'Minions, destroy these interlopers!', 16751, 1, 0, 0, 'Tyrannus Opening'),
('0', '-1658084', 'I do what i must. Please forgive me noble soldiers', 16628, 1, 0, 0, 'Jaina Opening'),
('0', '-1658085', 'You will have to battle your way through this cesspit on your own.', 17047, 0, 0, 0, 'Sylvanas Opening'),
('0', '-1658086', 'You will have to make your way across this quarry on your own.', 16629, 0, 0, 0, 'Jaina Opening'),
('0', '-1658087', 'Free any horde slaves that you come across. We will most certainly need there assistance in battling Tyrannus. I will gather reinforcements and join you on the other side of the quarry.', 17048, 0, 0, 0, 'Sylvanas Opening'),
('0', '-1658088', 'Free any Alliance slaves that you come across. We will most certainly need there assistance in battling Tyrannus. I will gather reinforcements and join you on the other side of the quarry.', 16630, 0, 0, 0, 'Jaina Opening');

DELETE FROM `vehicle_template_accessory` WHERE `entry` IN (36794);

UPDATE script_texts SET content_loc8='Воины Орды, в атаку!' WHERE entry = -1658076;
UPDATE script_texts SET content_loc8='Вам одним придется пройти через эту выгребную Яму' WHERE entry = -1658085;
UPDATE script_texts SET content_loc8='Жалкие слабаки' WHERE entry = -1658080;
UPDATE script_texts SET content_loc8='Освобождайте рабов - ордынцев, которые встретятся вам на пути. Они пригодятся нам в битве с Тиранием. Я соберу подкрепление и присоединюсь к вам на другой стороне рудника.' WHERE entry = -1658087;
UPDATE script_texts SET content_loc8='Герои Альянса, в атаку!' WHERE entry = -1658077;
UPDATE script_texts SET content_loc8='Нет! Ах ты чудовище!' WHERE entry = -1658081;
UPDATE script_texts SET content_loc8='Вам придется пройти через рудник без меня' WHERE entry = -1658084;
UPDATE script_texts SET content_loc8='Освобождайте рабов альянса, которые встретятся вам на пути. Они пригодятся нам в битве с Тиранием. Я соберу подкрепление и присоединюсь к вам на другой стороне рудника.' WHERE entry = -1658088;
UPDATE script_texts SET content_loc8='Чужаки проникли во владение господина. Поднять тревогу!' WHERE entry = -1658074;
UPDATE script_texts SET content_loc8='Последнее, что вам запомнится, - это агония.' WHERE entry = -1658078;
UPDATE script_texts SET content_loc8='Вот падаль. Вы даже для работы на рудниках не годитесь. Запомните свои последние минуты - скоро вы будете безмозглой нежитью.' WHERE entry = -1658075;
UPDATE script_texts SET content_loc8='Прислужники, убейте этих наглецов!' WHERE entry = -1658082;

DELETE FROM script_texts WHERE entry IN(-1658022, -1658023);
INSERT INTO script_texts (entry,content_default,sound,type,language,emote,comment) VALUES
(-1658022,'%s hurls a massive saronite boulder at you!',0,5,0,0,'garfrost EMOTE_THROW_SARONITE'),
(-1658023,'%s casts Deep Freeze at $N.',0,3,0,0,'garfrost EMOTE_DEEP_FREEZE');

UPDATE script_texts SET content_loc8='%s готовиться бросить в вас саронитом!' WHERE entry = -1658022;
UPDATE script_texts SET content_loc8='%s снимает глубокую заморозку с тебя.' WHERE entry = -1658023;

DELETE FROM `script_texts` WHERE entry = -1658071;
INSERT INTO `script_texts` (`entry`,`content_default`,`content_loc1`,`content_loc2`,`content_loc3`,`content_loc4`,`content_loc5`,`content_loc6`,`content_loc7`,`content_loc8`,`sound`,`type`,`language`,`emote`,`comment`) VALUES
(-1658071,'This way! We\'re about to mount an assault on the Scourgelord\'s location!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Сюда! Мы возьмем штурмом пристанище Повелителя Плети!',17153,1,0,0,'SAY_RESCOUD_HORDE_ALLIANCE');

UPDATE `script_texts` SET `content_loc8` = 'Все ваши усилия напрасны, чужаки, ибо Король Лич поставил меня во главе целой армии нежити! Берегитесь!' WHERE entry =  '-1658050';
UPDATE `script_texts` SET `content_loc8` = 'Настырные щенки! Вам не пройти в покои моего господина. Солдаты, уничтожить их!' WHERE entry =  '-1658051';
UPDATE `script_texts` SET `content_loc8` = 'Иней, останови их! Похорони их заживо!' WHERE entry =  '-1658052';
UPDATE `script_texts` SET `content_loc8` = 'Маленьких букашек тыща принести Гархладу пища... Ха-ха-ха-ха' WHERE entry =  '-1658001';
UPDATE `script_texts` SET `content_loc8` = 'Оставить на потом. Сейчас не хочу...' WHERE entry =  '-1658002';
UPDATE `script_texts` SET `content_loc8` = 'Этот кусок сейчас лучше не есть, глупый Гархлад. Плохой! Плохой!' WHERE entry =  '-1658003';
UPDATE `script_texts` SET `content_loc8` = 'Гархлад надеется, большие штаны чистые. Спасет от большого позора. Пока.' WHERE entry =  '-1658004';
UPDATE `script_texts` SET `content_loc8` = 'Топор слабый. Гархлад, сделает другую штуку и раздавит вас' WHERE entry =  '-1658005';
UPDATE `script_texts` SET `content_loc8` = 'Гархлад устал от жалких смертных. Сейчас ваши кости - леденец.' WHERE entry =  '-1658006';
UPDATE `script_texts` SET `content_loc8` = 'Его место займет другой. Вы попусту тратите время' WHERE entry =  '-1658007';
UPDATE `script_texts` SET `content_loc8` = 'Нельзя мешать нашей работе! Ик! Займемся ими!' WHERE entry =  '-1658010';
UPDATE `script_texts` SET `content_loc8` = 'О-хо-хо... Твои культяпки нам пригодятся!' WHERE entry =  '-1658011';
UPDATE `script_texts` SET `content_loc8` = 'У нас как раз кончались руки и ноги... Благодарю за вклад!' WHERE entry =  '-1658012';
UPDATE `script_texts` SET `content_loc8` = 'Хватит суетиться! Замри, пока я буду их взрывать!' WHERE entry =  '-1658013';
UPDATE `script_texts` SET `content_loc8` = 'Крик пускает Минометный обстрел' WHERE entry =  '-1658014';
UPDATE `script_texts` SET `content_loc8` = 'Скорей! Отрави их, пока они близко!' WHERE entry =  '-1658015';
UPDATE `script_texts` SET `content_loc8` = 'Нет же! Целься в этого! Да вот в этого!' WHERE entry =  '-1658016';
UPDATE `script_texts` SET `content_loc8` = 'Я... передумал... лучше в этого!' WHERE entry =  '-1658017';
UPDATE `script_texts` SET `content_loc8` = 'Да что ты к тому привязался? Этот опаснее, тупица!' WHERE entry =  '-1658018';
UPDATE `script_texts` SET `content_loc8` = 'Ик выпускает токсичную ядовитую звезду' WHERE entry =  '-1658020';
UPDATE `script_texts` SET `content_loc8` = 'Ик приследует вас!' WHERE entry =  '-1658021';
UPDATE `script_texts` SET `content_loc8` = 'Подождите! Не убивайте меня, умоляю! Я вам все расскажу!' WHERE entry =  '-1658030';
UPDATE `script_texts` SET `content_loc8` = 'Я не так наивна, чтобы верить твоим причитаниям. Но я выслушаю тебя.' WHERE entry =  '-1658031';
UPDATE `script_texts` SET `content_loc8` = 'Почему королева Банши должна выслушивать твое нытьё?' WHERE entry =  '-1658032';
UPDATE `script_texts` SET `content_loc8` = 'То, что вы ищете, находится в покоях господина, но чтобы попасть туда, вам надо убить Тирания. В Залах Отражений хранится Ледяная Скорбь. В клинке... сокрыта Истина!' WHERE entry =  '-1658033';
UPDATE `script_texts` SET `content_loc8` = 'Меч никто не охраняет? Не может быть.' WHERE entry =  '-1658034';
UPDATE `script_texts` SET `content_loc8` = 'Король Лич никогда не расстается со своим мечом, если ты мне лжешь...' WHERE entry =  '-1658035';
UPDATE `script_texts` SET `content_loc8` = 'Клянусь это правда! Прошу, не убивайте меня!!' WHERE entry =  '-1658036';
UPDATE `script_texts` SET `content_loc8` = 'Жалкая букашка! Тебя ждет лишь смерть!' WHERE entry =  '-1658037';
UPDATE `script_texts` SET `content_loc8` = 'Неет!...' WHERE entry =  '-1658038';
UPDATE `script_texts` SET `content_loc8` = 'Не думайте, что я так легко позволю вам пройти в покои господина. Сразитесь со мной, если посмеете.' WHERE entry =  '-1658039';
UPDATE `script_texts` SET `content_loc8` = 'Какая ужасная смерть... Идемте, герои, проверим, правду ли сказал гном. Если нам удастся завладеть Ледяной Скорбью, возможно, мы сумеем остановить Артаса.' WHERE entry =  '-1658040';
UPDATE `script_texts` SET `content_loc8` = 'Смерть, достойная предателя. Идемте, освободим рабов и увидим своими глазами, что хранится в покоях Короля Лича' WHERE entry =  '-1658041';
UPDATE `script_texts` SET `content_loc8` = 'Увы, бесстрашные герои, ваша навязчивость ускорила развязку. Вы слышите громыхание костей и скрежет стали за вашими спинами? Это предвестники скорой погибели!' WHERE entry =  '-1658053';
UPDATE `script_texts` SET `content_loc8` = 'Герои! Мы будем сдерживать нежить до последнего вздоха, сразитесь с повелителем плети!' WHERE entry =  '-1658054';
UPDATE `script_texts` SET `content_loc8` = 'Ха-ха-ха-ха.. кто бы мог ожидать такого от черни. Когда я с вами покончу, клинок господина насытится вашими душами. Умрите!' WHERE entry =  '-1658055';
UPDATE `script_texts` SET `content_loc8` = 'Я не подведу Короля Лича! Идите ко мне, и встретьте свою смерть!' WHERE entry =  '-1658056';
UPDATE `script_texts` SET `content_loc8` = 'Какое жалкое кривляние... Умри и не позорься!' WHERE entry =  '-1658057';
UPDATE `script_texts` SET `content_loc8` = 'Тебе надо было остаться в горах!' WHERE entry =  '-1658058';
UPDATE `script_texts` SET `content_loc8` = 'Не может быть! Иней... предупреди...' WHERE entry =  '-1658059';
UPDATE `script_texts` SET `content_loc8` = 'Иней, уничтожь этого глупца!' WHERE entry =  '-1658060';
UPDATE `script_texts` SET `content_loc8` = 'Иней пристально глядит на $N и готовится к ледяной атаке!' WHERE entry =  '-1658061';
UPDATE `script_texts` SET `content_loc8` = 'Меня переполняет мощь!' WHERE entry =  '-1658062';
UPDATE `script_texts` SET `content_loc8` = 'Повелитель плети Тираний ревет и выпускает темную энергию' WHERE entry =  '-1658063';
UPDATE `script_texts` SET `content_loc8` = 'Храбрые защитники! Мы обязаны вам своими жизнями, своей свободой. И хотя это слишком малая благодарность за ваше бесстрашие, знайте что мы по всюду разнесем весть о ваших деяниях. И о том свете что вы несете сквозь эти мрачные залы.' WHERE entry =  '-1658064';
UPDATE `script_texts` SET `content_loc8` = 'Ваши доблесные деяния ознаменуют союз против которого не выстоит даже сам Король - Лич. Союз Альянса и орды забывших о своих распрях и...' WHERE entry =  '-1658065';
UPDATE `script_texts` SET `content_loc8` = 'Ко мне, герои!' WHERE entry =  '-1658066';
UPDATE `script_texts` SET `content_loc8` = 'Укройтесь за мной! Скорее!' WHERE entry =  '-1658067';
UPDATE `script_texts` SET `content_loc8` = 'Королева льда улетела. Надо продолжать путь - цель уже близка.' WHERE entry =  '-1658068';
UPDATE `script_texts` SET `content_loc8` = 'Я думала, он никогда не заткнется. Но Синдрагоса заставила этого болтливого дурня умолкнуть. В Залы Отражений, герои!' WHERE entry =  '-1658069';
UPDATE `script_texts` SET `content_loc8` = 'Я... Я не смогла их спасти... Будь ты Проклят, Артас! Будь ты Проклят!' WHERE entry =  '-1658070';

UPDATE creature_template SET unit_flags = unit_flags|33554946, faction_A = 14, faction_H = 14, minlevel=82, maxlevel=82, modelid1='11686', modelid2='11686', ScriptName='npc_icicle_pos_trigger' WHERE entry = 36848;
UPDATE creature_template SET unit_flags = unit_flags|33554432, faction_A = 14, faction_H = 14, minlevel=82, maxlevel=82, modelid1='28470', modelid2='28470', ScriptName='npc_icicle_pos' WHERE entry = 36847;

DELETE FROM creature WHERE id = 36848;
INSERT INTO creature (guid,id,map,spawnMask,phaseMask,modelid,equipment_id,position_x,position_y,position_z,orientation,spawntimesecs,spawndist,currentwaypoint,curhealth,curmana,MovementType) VALUES
(9338263, 36848, 658, 3, 1, 0, 0, 984.327, -123.532, 606.169, 2.57989, 25, 0, 0, 1, 0, 0),
(9338265, 36848, 658, 3, 1, 0, 0, 954.823, -113.569, 595.607, 5.42302, 25, 0, 0, 1, 0, 0),
(9338267, 36848, 658, 3, 1, 0, 0, 954.823, -113.569, 595.607, 5.42302, 25, 0, 0, 1, 0, 0),
(9338269, 36848, 658, 3, 1, 0, 0, 971.897, -117.602, 598.33, 5.39946, 25, 0, 0, 1, 0, 0),
(9338271, 36848, 658, 3, 1, 0, 0, 995.016, -137.575, 614.409, 2.48956, 25, 0, 0, 1, 0, 0),
(9338273, 36848, 658, 3, 1, 0, 0, 1025.87, -131.319, 624.948, 0.694921, 25, 0, 0, 1, 0, 0),
(9338277, 36848, 658, 3, 1, 0, 0, 1010, -133.736, 621.063, 0.420031, 25, 0, 0, 1, 0, 0),
(9338279, 36848, 658, 3, 1, 0, 0, 1036.56, -112.112, 628.223, 0.954102, 25, 0, 0, 1, 0, 0),
(9338283, 36848, 658, 3, 1, 0, 0, 1050.57, -110.447, 630.219, 1.26041, 25, 0, 0, 1, 0,  0),
(9338285, 36848, 658, 3, 1, 0, 0, 1038.18, -122.872, 627.494, 3.71085, 25, 0, 0, 1, 0, 0),
(9338287, 36848, 658, 3, 1, 0, 0, 1027.82, -119.878, 626.062, 2.63486, 25, 0, 0, 1, 0, 0),
(9338291, 36848, 658, 3, 1, 0, 0, 1057.61, -109.538, 630.79, 4.20329, 25, 0, 0, 1, 0, 0),
(9338295, 36848, 658, 3, 1, 0, 0, 1053.46, -76.9363, 633.156, 2.283, 25, 0, 0, 1, 0, 0),
(9338299, 36848, 658, 3, 1, 0, 0, 1062.59, -79.955, 633.425, 1.07505, 25, 0, 0, 1, 0, 0),
(9338301, 36848, 658, 3, 1, 0, 0, 1058.77, -58.7953, 633.694, 1.4489, 25, 0, 0, 1, 0, 0),
(9338303, 36848, 658, 3, 1, 0, 0, 1070.26, -47.45, 633.68, 1.32717, 25, 0, 0, 1, 0, 0),
(9338305, 36848, 658, 3, 1, 0, 0, 1072.96, -31.9644, 633.386, 1.48425, 25, 0, 0, 1, 0, 0),
(9338311, 36848, 658, 3, 1, 0, 0, 1062.32, -34.7107, 633.884, 1.4332, 25, 0, 0, 1, 0, 0),
(9338317, 36848, 658, 3, 1, 0, 0, 1065.15, -13.7891, 633.829, 1.66253, 25, 0, 0, 1, 0, 0),
(9338323, 36848, 658, 3, 1, 0, 0, 1077.13, -12.6189, 633.002, 1.46933, 25, 0, 0, 1, 0, 0),
(9338329, 36848, 658, 3, 1, 0, 0, 1076.42, -0.124692, 634.261, 1.7395, 25, 0, 0, 1, 0, 0),
(9338331, 36848, 658, 3, 1, 0, 0, 1081.21, 17.3858, 633.042, 1.61776, 25, 0, 0, 1, 0, 0),
(9338333, 36848, 658, 3, 1, 0, 0, 1073.75, 22.9217, 632.193, 1.75521, 25, 0, 0, 1, 0, 0),
(9338335, 36848, 658, 3, 1, 0, 0, 1078.59, 38.6189, 629.612, 1.74578, 25, 0, 0, 1, 0, 0),
(9338337, 36848, 658, 3, 1, 0, 0, 1071.05, 38.5154, 629.762, 3.26632, 25, 0, 0, 1, 0, 0),
(9338341, 36848, 658, 3, 1, 0, 0, 1064.84, 54.1026, 631.51, 1.94449, 25, 0, 0, 1, 0, 0),
(9338349, 36848, 658, 3, 1, 0, 0, 1070.66, 61.8228, 632.008, 1.88244, 25, 0, 0, 1, 0, 0),
(9338345, 36848, 658, 3, 1, 0, 0, 1064.46, 67.7368, 631.604, 1.76778, 25, 0, 0, 1, 0, 0),
(9338347, 36848, 658, 3, 1, 0, 0, 1070.92, 79.0623, 631.02, 1.31931, 25, 0, 0, 1, 0, 0);


insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018880','36788','658','3','1','22196','0','497.319','247.856','528.792','6.24828','86400','0','0','161280','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019650','36794','658','3','1','27982','0','539.634','222.141','548.685','3.10669','86400','0','0','107848','4169','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022810','36788','658','3','1','22196','0','496.835','198.899','528.795','0','86400','0','0','161280','8814','0','0','0','0');

delete from creature where id = 36886 AND map = 658;
DELETE FROM creature_addon
WHERE creature_addon.guid IN
(SELECT guid FROM creature WHERE id = 36886);

insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018150','36886','658','3','1','26577','0','884.868','-21.4253','546.677','2.37365','86400','0','0','81900','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019760','36886','658','3','1','26577','0','843.929','-34.8941','556.895','1.93731','86400','0','0','81900','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020830','36886','658','3','1','26577','0','882.438','-31.2049','563.539','2.42601','86400','0','0','81900','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021840','36886','658','3','1','26577','0','807.835','-37.7014','548.812','1.93731','86400','0','0','81900','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022760','36886','658','3','1','26577','0','823.8','-29.1788','529.015','1.76278','86400','0','0','81900','3994','0','0','0','0');

delete FROM creature WHERE id IN (36764, 36765, 36766, 36767, 36770, 36771, 36772, 36773);
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2017980','36764','658','3','1','30369','0','720.755','166.148','511.188','4.90438','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018990','36764','658','3','1','30368','0','684.483','72.5347','491.383','1.22173','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019170','36764','658','3','1','30369','0','737.856','158.335','511.398','4.27606','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018390','36765','658','3','1','30373','0','683.92','-58.8646','507.507','3.35103','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019200','36765','658','3','1','30373','0','748.806','53.4028','463.444','1.8326','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021510','36766','658','3','1','30375','0','726.563','45.2847','450.561','0.523599','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022560','36766','658','3','1','30374','0','665.944','18.066','485.572','5.8294','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018730','36767','658','3','1','30378','0','701.566','8.72917','446.907','1.48353','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018050','36770','658','3','1','30382','0','575.082','247.102','509.09','4.85202','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018070','36770','658','3','1','30381','0','691.132','-46','486.064','4.67748','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018160','36770','658','3','1','30380','0','587.977','198.151','509.651','2.93215','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018610','36770','658','3','1','30381','0','750.149','-107.019','513.02','5.77704','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019320','36770','658','3','1','30383','0','581.865','-16.9219','512.681','2.77507','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019430','36770','658','3','1','30381','0','636.259','-70.6215','512.671','3.90954','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019450','36770','658','3','1','30381','0','630.219','277.049','507.59','2.47837','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019560','36770','658','3','1','30381','0','572.309','168.01','509.938','3.4383','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019900','36770','658','3','1','30382','0','834.316','-17.3299','509.567','4.01426','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020960','36770','658','3','1','30381','0','581.635','-5.30382','512.681','3.42085','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021180','36770','658','3','1','30383','0','637.498','182.351','507.01','1.13446','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021390','36770','658','3','1','30380','0','550.2','264.851','509.09','3.50811','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021660','36770','658','3','1','30383','0','721.189','-70.2917','492.521','4.2586','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022600','36770','658','3','1','30380','0','546.786','77.5','527.738','3.66519','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022780','36770','658','3','1','30383','0','721.995','-43.9149','479.963','5.35816','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018130','36771','658','3','1','30385','0','676.29','168.757','508.003','0.139626','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018230','36771','658','3','1','30385','0','725.977','149.089','511.345','1.11701','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018570','36771','658','3','1','30384','0','754.168','-95.066','512.83','0.069813','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019050','36771','658','3','1','30384','0','632.587','-59.5139','512.672','2.77507','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019460','36771','658','3','1','30384','0','591.67','-23.7361','512.714','5.61996','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020570','36771','658','3','1','30384','0','754.91','-23.8333','482.499','4.55531','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020670','36771','658','3','1','30384','0','777.953','6.66146','489.561','6.03884','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020760','36771','658','3','1','30384','0','661.323','210.665','507.632','4.74729','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022440','36771','658','3','1','30384','0','586.727','6.61111','512.674','3.35103','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022640','36771','658','3','1','30384','0','548.403','89.5608','525.462','2.75762','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022690','36771','658','3','1','30385','0','647.373','-112.151','513.411','2.70526','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022710','36771','658','3','1','30384','0','866.153','-11.3438','509.741','5.46288','86400','0','0','37800','3994','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2018180','36772','658','3','1','30387','0','607.217','-14.0799','512.685','4.81711','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019740','36772','658','3','1','30386','0','713.17','153.476','511.47','0.20944','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2019840','36772','658','3','1','30387','0','668.299','-15.4757','479.273','1.88496','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020580','36772','658','3','1','30386','0','815.554','-12.0174','509.567','5.5676','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020610','36772','658','3','1','30386','0','589.116','20.4201','512.699','2.07694','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020820','36772','658','3','1','30386','0','640.311','-83.4201','512.671','3.68265','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020840','36772','658','3','1','30386','0','559.29','64.3524','525.223','3.76991','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021080','36772','658','3','1','30387','0','647.554','192.082','507.505','2.77507','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021830','36772','658','3','1','30387','0','683.835','221.087','511.18','1.02974','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022290','36772','658','3','1','30386','0','753.399','-80.4878','512.683','0.10472','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022750','36772','658','3','1','30387','0','856.13','-17.7587','509.472','5.32325','86400','0','0','37800','0','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020370','36773','658','3','1','30390','0','668.898','46.1267','480.066','2.84489','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2020700','36773','658','3','1','30389','0','668.253','-37.0243','504.216','3.01942','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021240','36773','658','3','1','30388','0','654.733','144.181','507.854','1.01229','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021260','36773','658','3','1','30391','0','592.59','29.5521','512.252','2.56563','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021310','36773','658','3','1','30388','0','706.422','79.4757','472.946','1.91986','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021440','36773','658','3','1','30390','0','698.347','190.694','510.591','4.38078','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2021810','36773','658','3','1','30391','0','646.306','-99.0122','512.7','3.80482','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022020','36773','658','3','1','30389','0','758.743','-60.941','511.572','5.72468','86400','0','0','30240','8814','0','0','0','0');
insert into `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) values('2022490','36773','658','3','1','30389','0','884.335','6.31771','509.72','1.16937','86400','0','0','30240','8814','0','0','0','0');

DELETE FROM gameobject WHERE id = 201969 AND map = 658;
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1501970','201969','658','3','1','592.08','-22.4306','512.605','-3.01941','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1501980','201969','658','3','1','637.587','-70.7083','512.642','1.91986','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502010','201969','658','3','1','575.352','248.378','509.007','-2.70526','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502020','201969','658','3','1','548.127','90.8594','525.021','-2.63544','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502050','201969','658','3','1','546.736','78.5799','527.68','-2.79252','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502110','201969','658','3','1','855.941','-16.5764','509.491','-2.33874','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502150','201969','658','3','1','752.821','-94.8229','512.716','-1.36136','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502160','201969','658','3','1','752.497','-81.316','512.603','-0.523598','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502190','201969','658','3','1','550.918','265.731','509.007','3.08918','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502200','201969','658','3','1','758.177','-62.0313','511.524','0.017452','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502250','201969','658','3','1','655.58','144.47','507.822','2.33874','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502260','201969','658','3','1','646.318','-97.9375','512.605','-2.91469','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502280','201969','658','3','1','648.649','-112.335','513.084','1.58825','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502340','201969','658','3','1','833.667','-16.2899','509.484','-2.32129','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502390','201969','658','3','1','749.285','-107.773','512.901','-0.715585','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502410','201969','658','3','1','588.804','199.064','509.48','2.65289','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502420','201969','658','3','1','630.884','275.889','507.606','1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502460','201969','658','3','1','582.547','-17.8663','512.617','1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502470','201969','658','3','1','699.583','190.753','510.857','2.30383','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502500','201969','658','3','1','587.677','5.81597','512.588','1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502540','201969','658','3','1','636.401','182.17','507.123','-0.977383','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502590','201969','658','3','1','676.191','167.217','507.966','0.663223','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502610','201969','658','3','1','864.655','-11.4358','509.657','-1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502640','201969','658','3','1','883.674','5.69097','509.652','-0.191985','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502650','201969','658','3','1','682.894','220.568','511.024','-0.855211','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502670','201969','658','3','1','560.273','64.6684','524.846','2.42601','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502700','201969','658','3','1','814.389','-12.2188','509.484','-1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502710','201969','658','3','1','661.503','211.88','507.633','-2.68781','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502740','201969','658','3','1','633.323','-60.6597','512.589','1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502790','201969','658','3','1','582.417','-6.21354','512.597','1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502820','201969','658','3','1','608.422','-13.6319','512.602','2.32129','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502860','201969','658','3','1','573.201','168.451','509.855','2.67035','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502870','201969','658','3','1','631.651','110.535','510.694','1.309','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502900','201969','658','3','1','641.556','-83.934','512.588','1.71042','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502930','201969','658','3','1','593.021','28.6684','512.291','1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1502970','201969','658','3','1','590.087','19.8438','512.596','1.18682','0','0','0','1','6000','100','1');
insert into `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) values('1503020','201969','658','3','1','648.74','191.651','507.575','1.81514','0','0','0','1','6000','100','1');